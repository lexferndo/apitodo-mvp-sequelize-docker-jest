const { todo } = require('../models')

class todosController {

    static findAll = async (req, res, next) => {
        try {
            const data = await todo.findAll(req.query)

            res.status(200).json({
                message: 'Get All Todo',
                data: data
            })
        } catch (error) {
            next(error)
        }
    }

    static findOne = async (req, res, next) => {
        try {
            const data = await todo.findOne(req.params)
            res.status(200).json({
                message: 'Get Todo Success',
                data: data
            })
        } catch (error) {
            next(error)
        }
    }

    static create = async (req, res, next) => {
        try {
            const data = await todo.create(req.body)

            res.status(201).json({
                message: 'Create New Todo Success',
                data: data
            })
        } catch (error) {
            next(error)
        }
    }

    static update = async (req, res, next) => {
        try {
            const {id} = req.params

            const data = await todo.update(req.body, {
                where:{
                    id
                }
            })

            res.status(200).json({
                message: 'Todo Updated Successfully'
            })
        } catch (error) {
            next(error)
        }
    }

    static destroy = async (req, res, next) => {
        try {
            const {id} = req.params

            const data = await todo.destroy({
                where:{
                    id
                }
            })
            res.status(200).json({
                message:'Todo Deleted Successfully'
            })
        } catch (error) {
            next(error)
        }
    }
}

module.exports = todosController