const app = require('../app')
const request = require("supertest")

const {sequelize} = require("../models")
const {queryInterface} = sequelize


beforeAll((done) => {
    queryInterface.bulkInsert("todo", 
        [
            {
              id: 1,
              title: "Latihan Rakamin 1",
              createdAt: new Date(),
              updatedAt: new Date()
            },
            {
              id: 2,
              title: "Latihan Rakamin 2",
              createdAt: new Date(),
              updatedAt: new Date()
            },
            {
              id: 3,
              title: "Latihan Rakamin 3",
              createdAt: new Date(),
              updatedAt: new Date()
            }
          ]
    , {})
    .then(_ => {
        done()
    })
    .catch(err => {
        console.log(err)
        done(err)
    })
})


afterAll((done) => {

    queryInterface.bulkDelete("todo", null, {})
        .then(_ => {
            done()
        })
        .catch(err => {
            console.log(err)
            done(err)
        })
})


describe('Testing Todo', () => {

    test('Findall Todo', (done) => {
        request(app)
            .get("/todo")
            .expect('Content-Type', /json/)
            .expect(200)
            .then(response => {
                const {body, status} = response;

                expect(status).toEqual(200);
                expect(body.data.length).toEqual(3)

                const data = body.data[0]

                expect(data.title).toBe('Latihan Rakamin 1')
                done();
            })
            .catch(err => {
                console.log(err)
                done(err)
            })
    })

    test('Find detail Todo', (done) => {
        request(app)
            .get('/todo/1')
            .expect('Content-Type', /json/)
            .expect(200)
            .then(response => {
                const { status } = response
                expect(status).toEqual(200)
                done()
            })
            .catch(err => {
                console.log(err)
                done(err)
            })
    })

    test('Create Todo', (done) => {
        const dummy = ({ 
            id: 4,
            title: 'Latihan Rakamin 4' 
        })
        request(app)
            .post('/todo')
            .send(dummy)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(201)
            .then(response => {
                const { body } = response

                expect(body.message).toEqual('Create New Todo Success')
                done()
            })
            .catch(err => {
                console.log(err)
                done(err)
            })
    })

    test('Update Todo', (done) => {
        const dummy = { title: 'Latihan Rakamin 1' }
        request(app)
            .put('/todo/1')
            .send(dummy)
            .set('Accept', 'application/json')
            .expect(200)
            .then(response => {
                const { body, status } = response

                expect(body.message).toEqual('Todo Updated Successfully')
                done()
            })
            .catch(err => {
                console.log(err)
                done(err)
            })
    })

    test('Delete Todo', (done) => {
        request(app)
            .delete('/todo/1')
            .expect('Content-Type', /json/)
            .expect(200)
            .then(response => {
                const { body, status } = response
                expect(body.message).toEqual('Todo Deleted Successfully')
                done()
            })
            .catch(err => {
                console(err)
                done(err)
            })
    })
})