const express = require('express')
const router = express.Router()

const todosController = require('../controller/todoController')

router.get('/', todosController.findAll)
router.get('/:id', todosController.findOne)
router.post('/', todosController.create)
router.put('/:id', todosController.update)
router.delete('/:id', todosController.destroy)



module.exports = router