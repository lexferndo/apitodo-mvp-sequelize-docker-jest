require('dotenv').config()
const PORT = process.env.PORT

const express = require('express')
const app = express()

const bodyParser = require('body-parser')
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

const morgan = require('morgan')
app.use(morgan('tiny'))

const todos = require('./router/todoRouter')
app.use('/todo', todos)

if (process.env.NODE_ENV !== 'test') {
    app.listen(PORT, () => {
      console.log(`App is Listening On:${PORT}`)
    });
  }

module.exports = app